//
//  SponsoredCalculator.h
//  OOPDemo
//
//  Created by James Cash on 04-05-16.
//  Copyright © 2016 Occasionally Cogent. All rights reserved.
//

#import "Calculator.h"

@interface SponsoredCalculator : Calculator

@property (strong,nonatomic) NSString *sponsor;

- (instancetype)initWithMeaning:(NSInteger)meaning andSponsor:(NSString*)sponsor;

@end
