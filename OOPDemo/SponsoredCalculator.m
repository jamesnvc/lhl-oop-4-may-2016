//
//  SponsoredCalculator.m
//  OOPDemo
//
//  Created by James Cash on 04-05-16.
//  Copyright © 2016 Occasionally Cogent. All rights reserved.
//

#import "SponsoredCalculator.h"

@implementation SponsoredCalculator

- (instancetype)initWithMeaning:(NSInteger)meaning andSponsor:(NSString*)sponsor;
{
    self = [super initWithMeaning:meaning];
    if (self) {
        _sponsor = sponsor;
    }
    return self;
}

- (NSInteger)addMeaning:(NSInteger)x
{
    if (self.sponsor) {
        NSLog(@"This addition brought to you by %@", self.sponsor);
    } else {
        NSLog(@"This could be your advertisment; sponsor us today");
    }
    NSInteger superResult = [super addMeaning:x*2];
    return superResult*10;
}

@end
