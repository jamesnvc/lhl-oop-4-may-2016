//
//  main.m
//  OOPDemo
//
//  Created by James Cash on 04-05-16.
//  Copyright © 2016 Occasionally Cogent. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "Calculator.h"
#import "SponsoredCalculator.h"

int main(int argc, const char * argv[]) {
    @autoreleasepool {
        Calculator *calc = [[Calculator alloc] init];
        NSInteger x = 1;
        NSInteger result = [calc addMeaning:x];
        NSLog(@"x = %ld ; x + meaning = %ld", x, result);

        SponsoredCalculator *sponsoredCalc = [[SponsoredCalculator alloc] initWithMeaning:37 andSponsor:@"Lighthouse"];
        NSInteger sponsorResult = [sponsoredCalc addMeaning:x];
        NSLog(@"sponsored: x = %ld ; x + meaning = %ld", x, sponsorResult);
        sponsoredCalc.meaning = 1;
        NSLog(@"sponsored: x = %ld ; x + meaning = %ld", x, [sponsoredCalc addMeaning:x]);

        SponsoredCalculator *calc2 = [[SponsoredCalculator alloc] init];
        NSLog(@"Sponsored 2: x = %ld, x + meaning = %ld", x, [calc2 addMeaning:x]);

    }
    return 0;
}
