//
//  Calculator.m
//  OOPDemo
//
//  Created by James Cash on 04-05-16.
//  Copyright © 2016 Occasionally Cogent. All rights reserved.
//

#import "Calculator.h"

@implementation Calculator

- (instancetype)init
{
    self = [super init];
    if (self) {
        _meaning = 42;
    }
    return self;
}

- (instancetype)initWithMeaning:(NSInteger)meaning
{
    self = [super init];
    if (self) {
        _meaning = meaning;
    }
    return self;
}

//- (void)setMeaning:(NSInteger)meaning
//{
//    if (meaning % 2 == 1) {
//        return;
//    }
//    _meaning = meaning;
//}

- (NSInteger)addMeaning:(NSInteger)x
{
    return x + self.meaning;
}

@end
