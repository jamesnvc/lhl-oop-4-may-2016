//
//  Calculator.h
//  OOPDemo
//
//  Created by James Cash on 04-05-16.
//  Copyright © 2016 Occasionally Cogent. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface Calculator : NSObject

@property (nonatomic,assign) NSInteger meaning;

- (instancetype)initWithMeaning:(NSInteger)meaning;
- (NSInteger)addMeaning:(NSInteger)x;

@end
